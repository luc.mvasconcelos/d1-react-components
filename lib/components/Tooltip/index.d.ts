import React from 'react';
export interface Props {
    content?: string;
    color?: string;
    backgroundColor?: string;
    animation?: boolean;
}
declare const TooltipWrapper: React.FC<Props>;
export { TooltipWrapper as Tooltip };
