import React from 'react';
import { Props as IconProps } from '../Icon';
export interface Props {
    backgroundColor?: string;
    color?: string;
    ghost?: boolean;
    onClick?: () => void;
    disabled?: boolean;
    children?: React.ReactNode;
    icon?: React.FC<IconProps>;
    fontSize?: string;
}
declare const ButtonWrapper: React.FC<Props>;
export { ButtonWrapper as Button };
