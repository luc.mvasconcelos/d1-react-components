import React from "react";
export interface Props {
    width?: string;
    height?: string;
    acme?: boolean;
}
declare const LogoWrapper: React.FC<Props>;
export { LogoWrapper as Logo };
