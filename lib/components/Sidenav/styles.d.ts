import { Props } from '.';
export declare const Container: import("styled-components").StyledComponent<"div", any, Props, never>;
export declare const Item: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const Footer: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const Logo: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const Separator: import("styled-components").StyledComponent<"div", any, {}, never>;
