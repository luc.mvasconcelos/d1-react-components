import React from 'react';
import { Item, Footer, Logo, Separator } from './styles';
export interface Props {
}
declare const SidenavWraper: React.FC<Props>;
export { SidenavWraper as SideNav, Item as SidenavItem, Footer as SidenavFooter, Logo as SidenavLogo, Separator as SidenavSeparator, };
