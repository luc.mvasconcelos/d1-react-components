/// <reference types="react" />
export interface Props {
    size?: string;
    color?: string;
}
export declare const ChartPieIcon: (props: Props) => JSX.Element;
export declare const RocketIcon: (props: Props) => JSX.Element;
export declare const UserFriendIcon: (props: Props) => JSX.Element;
export declare const GemIcon: (props: Props) => JSX.Element;
export declare const SignOutIcon: (props: Props) => JSX.Element;
export declare const PlusIcon: (props: Props) => JSX.Element;
export declare const ExchangeIcon: (props: Props) => JSX.Element;
export declare const ExternalLinkIcon: (props: Props) => JSX.Element;
export declare const ToolsIcon: (props: Props) => JSX.Element;
export declare const CloudIcon: (props: Props) => JSX.Element;
export declare const ThIcon: (props: Props) => JSX.Element;
export declare const PaperPlaneIcon: (props: Props) => JSX.Element;
export declare const PenIcon: (props: Props) => JSX.Element;
export declare const SearchIcon: (props: Props) => JSX.Element;
export declare const BedIcon: (props: Props) => JSX.Element;
export declare const PlayCircleIcon: (props: Props) => JSX.Element;
export declare const CheckIcon: (props: Props) => JSX.Element;
export declare const DatabaseIcon: (props: Props) => JSX.Element;
export declare const CircleIcon: (props: Props) => JSX.Element;
export declare const QuestionIcon: (props: Props) => JSX.Element;
