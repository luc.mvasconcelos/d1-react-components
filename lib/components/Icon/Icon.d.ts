import React from 'react';
import 'line-awesome/dist/line-awesome/css/line-awesome.min.css';
export interface Props {
    className: string;
    size?: string;
    color?: string;
}
declare const IconWrapper: React.FC<Props>;
export { IconWrapper as Icon };
