import React from 'react';
export interface Props {
    titles: string[];
    data: any[][];
    backgroudColor?: string;
}
declare const TableWrapper: React.FC<Props>;
export { TableWrapper as Table };
