import React from 'react';
export interface Props {
    placeholder?: string;
    value?: string;
    search?: boolean;
    onChange?: (e: any) => void;
}
declare const InputWrapper: React.FC<Props>;
export { InputWrapper as Input };
