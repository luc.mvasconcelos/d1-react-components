import React from 'react';
export interface Props {
    color?: string;
    value?: string | number;
    selectedColor?: string;
    onChange?: (e: any) => void;
    children?: React.ReactNode;
    fontSize?: string;
    checked?: boolean;
}
declare const ItemWrapper: React.FC<Props>;
export { ItemWrapper as RadioItem };
