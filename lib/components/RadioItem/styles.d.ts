import { Props } from './index';
export declare const Item: import("styled-components").StyledComponent<"div", any, Props, never>;
export declare const RadioInput: import("styled-components").StyledComponent<"input", any, {}, never>;
