import React from 'react';
export interface Props {
    title: string;
    footer?: React.ReactNode;
    active: boolean;
    hideModal: () => void;
}
declare const ModalWrapper: React.FC<Props>;
export { ModalWrapper as Modal };
