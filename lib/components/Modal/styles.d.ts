export declare const ModalBlock: import("styled-components").StyledComponent<"div", any, {
    active: boolean;
}, never>;
export declare const ModalOverlay: import("styled-components").StyledComponent<"a", any, {}, never>;
export declare const ModalContainer: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const ModalBody: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const ModalHeader: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const ModalTitle: import("styled-components").StyledComponent<"span", any, {}, never>;
export declare const ModalFooter: import("styled-components").StyledComponent<"div", any, {}, never>;
export declare const Button: import("styled-components").StyledComponent<"button", any, {}, never>;
