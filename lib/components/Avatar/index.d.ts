import React from 'react';
export interface Props {
    backgroundColor?: string;
    color?: string;
    size?: string;
}
declare const AvatarWrapper: React.FC<Props>;
export { AvatarWrapper as Avatar };
