Available Scripts
In the project directory, you can run:

"yarn storybook"
Runs the lib in the development mode.
Open http://localhost:6006 to view it in the browser.

"yarn build"
Builds the lib for production to the lib folder.

"yarn build-storybook"
Builds the lib statically to web deployment on the storybook-static folder.

Online Demo:
https://blissful-ardinghelli-1f33ef.netlify.app/
