import { create } from '@storybook/theming';

export default create({
  base: 'light',
  brandTitle: 'D1 React Components',
  brandUrl: 'https://www.d1.cx/',
  brandImage: 'https://uploads-ssl.webflow.com/5efcbbdc5b5341ae33f3c9bd/5f872c846b74260070b424d0_d1-logo.svg'
});