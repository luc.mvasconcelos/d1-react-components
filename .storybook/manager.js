import { addons } from '@storybook/addons';
import d1Theme from './d1Theme';

addons.setConfig({
  theme: d1Theme,
});