import React, { useState } from 'react'
import { storiesOf } from '@storybook/react'

import { RadioGroup, RadioItem, PenIcon, BedIcon, ThIcon, Avatar } from '../'

storiesOf('RadioGroup', module).add('Default', () => {
    const [selectedValue, setSelectedValue] = useState(1)
    return (
        <RadioGroup>
            <RadioItem
                checked={selectedValue == 1}
                value={1}
                onChange={(e) => setSelectedValue(e.target.value)}
            >
                <ThIcon size={'16px'} color="#D190DD" />
                <span>
                    Option 1
                </span>
                <Avatar backgroundColor="#E4E6F1" color="#9196AB">
                    1
                </Avatar>
            </RadioItem>
            <RadioItem
                checked={selectedValue == 2}
                value={2}
                onChange={(e) => {
                    console.log(e)
                    setSelectedValue(e.target.value)
                }}
            >
                <PenIcon size={'16px'} color="#3FA8F4" />
                <span>
                    Option 2
                </span>
                <Avatar backgroundColor="#E4E6F1" color="#9196AB">
                    2
                </Avatar>
            </RadioItem>
            <RadioItem
                checked={selectedValue == 3}
                value={3}
                onChange={(e) => setSelectedValue(e.target.value)}
            >
                <BedIcon size={'16px'} color="#EBBD3E" />
                <span>
                    Option 3
                </span>
                <Avatar backgroundColor="#E4E6F1" color="#9196AB">
                    3
                </Avatar>
            </RadioItem>
        </RadioGroup>
    )
})
