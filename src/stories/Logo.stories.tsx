import React from 'react'
import { storiesOf } from '@storybook/react'

import { Logo } from '../'

storiesOf('Logo', module)
    .add('Default', () => <Logo width="30px"/>)
    .add('ACME', () => (
        <div
            style={{
                padding: '6px 24px 3px 24px',
                backgroundColor: '#EBEEF6',
                width: 'fit-content',
                border: '1px solid #EBEEF6',
                borderRadius: '5px',
            }}
        >
            <Logo acme width="30px"/>
        </div>
    ))
