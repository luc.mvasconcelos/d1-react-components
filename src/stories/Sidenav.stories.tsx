import React from 'react'
import { storiesOf } from '@storybook/react'

import {
    SideNav,
    PlusIcon,
    SidenavItem,
    GemIcon,
    RocketIcon,
    SignOutIcon,
    SidenavFooter,
    SidenavLogo,
    SidenavSeparator,
} from '../'

storiesOf('Sidenav', module).add('Default', () => (
    <SideNav>
        <SidenavLogo>
            <img
                src={
                    'https://uploads-ssl.webflow.com/5efcbbdc5b5341ae33f3c9bd/5f872eea2ee9cf8ca58522a2_d1-logo-branca.svg'
                }
                alt="logo"
            />
        </SidenavLogo>
        <SidenavItem>
            <PlusIcon size="21px" />
        </SidenavItem>
        <SidenavItem>
            <GemIcon size="21px" />
        </SidenavItem>
        <SidenavItem>
            <RocketIcon size="21px" />
        </SidenavItem>
        <SidenavItem>
            <SignOutIcon size="21px" />
        </SidenavItem>
        <SidenavItem>
            <SidenavSeparator />
        </SidenavItem>

        <SidenavFooter>
            <SidenavItem>
                <GemIcon size="21px" />
            </SidenavItem>
            <SidenavItem>
                <PlusIcon size="21px" />
            </SidenavItem>
        </SidenavFooter>
    </SideNav>
))
