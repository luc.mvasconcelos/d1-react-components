import React from 'react'
import { storiesOf } from '@storybook/react'

import {
    ChartPieIcon,
    RocketIcon,
    UserFriendIcon,
    GemIcon,
    SignOutIcon,
    PlusIcon,
    ExchangeIcon,
    ToolsIcon,
    CloudIcon,
    ThIcon,
    PaperPlaneIcon,
    PenIcon,
    SearchIcon,
    BedIcon,
    PlayCircleIcon,
    CheckIcon,
    DatabaseIcon,
    CircleIcon,
} from '../index'

storiesOf('List of Icons', module)
    .add('Icons', () => (
        <>
            {' '}
            <ChartPieIcon /> <RocketIcon /> <UserFriendIcon /> <GemIcon />{' '}
            <SignOutIcon /> <PlusIcon /> <ExchangeIcon />
            <ToolsIcon /> <CloudIcon /> <ThIcon /> <PaperPlaneIcon />{' '}
            <PenIcon /> <SearchIcon /> <BedIcon /> <PlayCircleIcon />{' '}
            <CheckIcon /> <DatabaseIcon /> <CircleIcon />{' '}
        </>
    ))
    .add('Color and Size', () => (
        <>
            {' '}
            <RocketIcon size="64px" color="blue" />{' '}
            <RocketIcon size="44px" color="red" />
            <RocketIcon size="24px" />{' '}
        </>
    ))
    .add('Union', () => (
        <>
            <CloudIcon />
            <span
                style={{ marginLeft: '-10.5px', marginTop: '4px', zIndex: 2 }}
            >
                <DatabaseIcon size="8px" />
            </span>{' '}
        </>
    ))
