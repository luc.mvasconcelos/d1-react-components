import React, { useState } from 'react'
import { storiesOf } from '@storybook/react'

import { Modal, Button, Input } from '../'

storiesOf('Modal', module).add('Default', () => {
    const [visible, setVisible] = useState(false)
    return (
        <>
            {' '}
            <Button onClick={() => setVisible(true)}>Open Modal</Button>{' '}
            <Modal
                footer={
                    <>
                        {' '}
                        <Button onClick={() => setVisible(false)} ghost>
                            Continuar
                        </Button>{' '}
                        <Button onClick={() => setVisible(false)} ghost>
                            Cancelar
                        </Button>{' '}
                    </>
                }
                title="Modal"
                active={visible}
                hideModal={() => setVisible(false)}
            >
                Content
            </Modal>
        </>
    )
})
