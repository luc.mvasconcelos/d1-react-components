import React from 'react'
import { storiesOf } from '@storybook/react'

import { Button, PlusIcon } from '../'

storiesOf('Button', module)
    .add('Default', () => <Button>Default</Button>)
    .add('Ghost', () => <Button ghost>Ghost</Button>)
    .add('Disabled', () => <Button disabled>Disabled</Button>)
    .add('With Icon', () => <Button icon={PlusIcon}> Nova Jornada</Button>)
