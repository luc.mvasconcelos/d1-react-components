import React, { useState } from 'react'
import { storiesOf } from '@storybook/react'

import { Input } from '..'

storiesOf('Input', module)
    .add('Default', () => {
        const [value, setValue] = useState('')
        return (
            <Input
                value={value}
                placeholder="Digite..."
                onChange={(e) => {
                    console.log(`e.target.value`, e.target.value)
                    setValue(e.target.value)
                }}
            />
        )
    })
    .add('Search', () => {
        const [value, setValue] = useState('')
        return (
            <Input
                value={value}
                search
                onChange={(e) => {
                    console.log(`e.target.value`, e.target.value)
                    setValue(e.target.value)
                }}
            />
        )
    })
