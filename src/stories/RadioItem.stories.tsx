import React from 'react'
import { storiesOf } from '@storybook/react'

import { RadioItem, PlusIcon } from '../'

storiesOf('Radio', module).add('Default', () => {
    const [check, setCheck] = React.useState(false)
    return (
        <RadioItem
            checked={check}
            value={'vteste'}
            onChange={(e) => setCheck((prev) => !prev)}
        >
            <PlusIcon size={'13px'} />
            Default
        </RadioItem>
    )
})
