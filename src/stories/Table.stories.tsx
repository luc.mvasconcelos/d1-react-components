import React from 'react'
import { storiesOf } from '@storybook/react'

import { Table } from '../'

storiesOf('Table', module).add('Default', () => {
    const titles = ['A', 'B', 'C']
    const data = [
        ['AA', 'BB', 'CC'],
        ['AAA', 'BBB', 'CCC'],
        ['AAAA', 'BBBB', 'CCCC'],
    ]
    return <Table titles={titles} data={data} backgroudColor="#EBEEF6" />
})
