import React from 'react'
import { storiesOf } from '@storybook/react'

import { Tooltip, Avatar } from '../'

storiesOf('Tooltip', module)
    .add('Default', () => (
        <Tooltip content={'This is a Tooltip!'}>
            <span style={{ cursor: 'pointer' }}>Hover me!</span>
        </Tooltip>
    ))
    .add('Change Color', () => (
        <Tooltip
            content={'This is a Tooltip!'}
            color="#9196AB"
            backgroundColor="#EBEEF6"
        >
            <span style={{ cursor: 'pointer' }}>Hover me!</span>
        </Tooltip>
    ))
    .add('Animation', () => (
        <Tooltip content={'This is a Tooltip!'} animation>
            <span style={{ cursor: 'pointer' }}>Hover me!</span>
        </Tooltip>
    ))
    .add('With Avatar', () => (
        <Tooltip content={'This is a Tooltip!'}>
            <Avatar>A</Avatar>
        </Tooltip>
    ));
