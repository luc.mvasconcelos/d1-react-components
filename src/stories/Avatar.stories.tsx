import React from 'react'
import { storiesOf } from '@storybook/react'

import { Avatar, PlusIcon } from '..'

storiesOf('Avatar', module)
    .add('Default', () => <Avatar>A</Avatar>)
    .add('WithIcon', () => (
        <Avatar>
            <PlusIcon size="13px" />
        </Avatar>
    ))
    .add('WithNumver', () => <Avatar>12</Avatar>)
