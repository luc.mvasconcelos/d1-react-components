import { createGlobalStyle } from 'styled-components'
import Gotham from './assets/fonts/GothamPro.woff2'
import GothamBold from './assets/fonts/GothamPro-Bold.woff2'

export const GlobalStyle = createGlobalStyle`
@font-face {
    font-family: Gotham;
    src: url(${Gotham}) format("woff2");
  }

  @font-face {
    font-family: Gotham;
    src: url(${GothamBold}) format("woff2");
    font-weight: bold;
  }

  * {
    font-family: Gotham;
    font-size: 13px;
  }
`
