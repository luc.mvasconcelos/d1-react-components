import styled, { css } from 'styled-components'
import { lighten, darken, readableColor, setLightness } from 'polished'

import { Props } from './index'

export const Container = styled.img<Props>`
    width: ${(props) => props.width};
    height: ${(props) => props.height};
`
