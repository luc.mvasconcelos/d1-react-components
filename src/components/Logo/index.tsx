import React from "react";
import D1Logo from "../../assets/images/logotipo.png";
import AcmeLogo from "../../assets/images/acmelogo.png";



import { Container } from "./styles";

export interface Props {
  width?: string;
  height?: string;
  acme?: boolean;
}

const LogoWrapper: React.FC<Props> = ({
  width="90px",
  height,
  acme = false,
  ...rest
}) => {
  return (
    <Container
      src={acme ? AcmeLogo : D1Logo}
      width={width}
      acme={acme}
      {...rest}
    />
  );
};

export { LogoWrapper as Logo };
