import React, { Ref } from 'react'

import { Container } from './styles'
import { RadioItem } from '../RadioItem'

export interface Props {
    children?: React.ReactNode
}

const ButtonWrapper: React.FC<Props> = ({ children, ...rest }) => {
    return <Container {...rest}>{children}</Container>
}

export { ButtonWrapper as RadioGroup }
