import styled from 'styled-components'

import { Props } from './index'

export const Container = styled.div<Props>`
    height: auto;
    width: fit-content;
    box-sizing: border-box;
`
