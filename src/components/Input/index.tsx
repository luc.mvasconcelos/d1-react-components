import React from 'react'

import { Container, Wraper } from './styles'
import { SearchIcon } from '../Icon'

export interface Props {
    placeholder?: string
    value?: string
    search?: boolean
    onChange?: (e: any) => void
}

const InputWrapper: React.FC<Props> = ({
    placeholder = 'Buscar',
    value,
    search = false,
    ...rest
}) => {
    return (
        <Wraper>
            {search && <SearchIcon size="14px" color="#117EFF" />}
            <Container
                placeholder={placeholder}
                search={search}
                value={value}
                {...rest}
            />
        </Wraper>
    )
}

export { InputWrapper as Input }
