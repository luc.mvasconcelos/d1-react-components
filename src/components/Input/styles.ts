import styled, { css } from 'styled-components'

import { Props } from './index'

export const Wraper = styled.div<Props>`
 position: relative;
 i {
   position: absolute;
   top: 12px;
   left 11px;
 }
`

export const Container = styled.input<Props>`
    width: 296px;
    height: 35px;
    line-height: 35px;
    border: 1px solid #cccfde;
    border-radius: 5px;
    background-color: #ffffff;
    color: #9196ab;
    transition: box-shadow 0.3s;
    padding-left: ${(props) => (props.search ? '32px' : '12px')};

    &:hover {
        box-shadow: 0 0 11px rgba(33, 33, 33, 0.2);
    }
    outline: none;

    &::placeholder {
        color: #9196ab;
        line-height: 35px;
    }
`
