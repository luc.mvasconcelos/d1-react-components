import React from 'react'

import { Container } from './styles'

export interface Props {
    titles: string[]
    data: any[][]
    backgroudColor?: string
}

const TableWrapper: React.FC<Props> = ({
    data,
    titles,
    backgroudColor = '#FFFFFF',
    ...rest
}) => {
    return (
        <Container
            data={data}
            titles={titles}
            backgroudColor={backgroudColor}
            {...rest}
        >
            <thead>
                <tr>
                    {titles.map((title, index) => (
                        <th key={index}>{title}</th>
                    ))}
                </tr>
            </thead>
            <tbody>
                {data.map((item, index) => (
                    <tr key={index}>
                        {item.map((element, index) => (
                            <td key={index}>{element}</td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </Container>
    )
}

export { TableWrapper as Table }
