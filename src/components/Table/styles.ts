import styled from 'styled-components'

import { Props } from './index'

export const Container = styled.table<Props>`
    border: none;

    border-collapse: separate;
    border-spacing: 0 10px;
    td,
    th {
        border: none;
        font-weight: normal;
    }

    td {
        padding: 16px;
        padding-right: 84px;
        color: #3e4157;
    }

    th {
        padding-left: 16px;
    }

    tbody * {
        background-color: ${(props) => props.backgroudColor};
    }

    & tr > td:first-child {
        font-weight: bold;
        color: #3e4157;
    }

    thead > tr {
        background-color: transparent;
        color: #9196ab;
    }
    th {
        text-align: left;
    }
`
