import React from 'react'

import {
    ModalBlock,
    ModalBody,
    ModalContainer,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    ModalTitle,
} from './styles'

export interface Props {
    title: string
    footer?: React.ReactNode
    active: boolean
    hideModal: () => void
}

const ModalWrapper: React.FC<Props> = ({
    children,
    title,
    footer,
    active,
    hideModal,
    ...rest
}) => {
    return (
        <>
            {active && (
                <ModalBlock active={active}>
                    <ModalOverlay onClick={() => hideModal()}></ModalOverlay>
                    <ModalContainer>
                        <ModalHeader>
                            <ModalTitle>{title}</ModalTitle>
                        </ModalHeader>
                        <ModalBody>{children}</ModalBody>
                        <ModalFooter>{footer}</ModalFooter>
                    </ModalContainer>
                </ModalBlock>
            )}
        </>
    )
}

export { ModalWrapper as Modal }
