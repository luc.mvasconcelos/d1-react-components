import React from 'react'

import { Container } from './styles'
import { Props as IconProps } from '../Icon'

export interface Props {
    backgroundColor?: string
    color?: string
    ghost?: boolean
    onClick?: () => void
    disabled?: boolean
    children?: React.ReactNode
    icon?: React.FC<IconProps>
    fontSize?: string
}

const ButtonWrapper: React.FC<Props> = ({
    children,
    backgroundColor = '#117EFF',
    color = '#fff',
    ghost = false,
    icon,
    fontSize = '13px',
    ...rest
}) => {
    return (
        <Container
            backgroundColor={backgroundColor}
            color={color}
            ghost={ghost}
            fontSize={fontSize}
            {...rest}
        >
            {icon ? icon({ color, size: fontSize }) : null}
            {children}
        </Container>
    )
}

export { ButtonWrapper as Button }
