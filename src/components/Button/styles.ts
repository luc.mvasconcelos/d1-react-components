import styled, { css } from 'styled-components'
import { lighten, darken, readableColor, setLightness } from 'polished'

import { Props } from '.'

export const Container = styled.button<Props>`
    background-color: ${(props) =>
        props.ghost ? 'transparent' : props.backgroundColor};
    color: ${({ color, ghost }: Props) => (ghost ? '#3E4157' : color)};
    font-size: ${(props) => props.fontSize};
    ${(props) =>
        !props.ghost &&
        css`
            font-weight: bold;
        `}

    padding: 10px 20px;

    border-radius: 4px;
    border: none;

    cursor: pointer;

    &:disabled {
        opacity: 0.7;
        cursor: not-allowed;
    }

    &:hover:not(:disabled) {
        background-image: ${(props) =>
            props.ghost
                ? 'transparent'
                : `linear-gradient(to right, #00E1FF, ${props.backgroundColor})`};
        ${(props) =>
            props.ghost &&
            css`
                color: ${props.backgroundColor};
                font-weight: bold;
            `}
    }

    :active:not(:disabled) {
        background-image: ${(props) =>
            props.ghost
                ? 'transparent'
                : `linear-gradient(to right, #00E1FF, ${props.backgroundColor})`};
        ${(props) =>
            props.ghost &&
            css`
                color: ${props.backgroundColor};
                font-weight: bold;
            `}
    }
`
