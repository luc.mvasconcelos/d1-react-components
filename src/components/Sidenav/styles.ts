import styled from 'styled-components'

import { Props } from '.'

export const Container = styled.div<Props>`
    background-color: #1a1731;
    width: 71px;
    height: 100vh;
    margin-top: 0;
    padding: 25px 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;
    border-top: 3px solid;
    border-bottom: 3px solid;
    border-image-slice: 1;
    border-image-source: linear-gradient(to right, #00e1ff, #117eff);
`

export const Item = styled.div`
    text-align: center;
    & {
        display: inline-block;
    }
    text-decoration: none;
    color: #e4e6f1;
    width: 100%;
    padding: 12px 0;
    cursor: pointer;
    display: flex;
    &:hover{
        color: #117EFF;
    }
`

export const Footer = styled.div`
    position: absolute;
    bottom: 0;
    margin-bottom: 21px;
`

export const Logo = styled.div`
    width: 34px;
    padding-bottom: 56px;
    padding-top: 10px;
    img {
        width: 100%;
        height: auto;
    }
`

export const Separator = styled.div`
    display: inline-block;
    width: 21px;
    height: 1px;
    background: transparent linear-gradient(90deg, #00e1ff 0%, #117eff 100%) 0%
        0% no-repeat padding-box;
`
