import React from 'react'

import { Container, Item, Footer, Logo, Separator } from './styles'

export interface Props {}

const SidenavWraper: React.FC<Props> = ({ children, ...rest }) => {
    return <Container {...rest}>{children}</Container>
}

export {
    SidenavWraper as SideNav,
    Item as SidenavItem,
    Footer as SidenavFooter,
    Logo as SidenavLogo,
    Separator as SidenavSeparator,
}
