import React from 'react'
import { Icon } from './Icon'

export interface Props {
    size?: string
    color?: string
}

export const ChartPieIcon = (props: Props) => (
    <Icon className="las la-chart-pie la-6x" {...props} />
)
export const RocketIcon = (props: Props) => (
    <Icon className="las la-rocket la-6x" {...props} />
)
export const UserFriendIcon = (props: Props) => (
    <Icon className="las la-user-friends la-6x" {...props} />
)
export const GemIcon = (props: Props) => (
    <Icon className="las la-gem la-6x" {...props} />
)
export const SignOutIcon = (props: Props) => (
    <Icon className="las la-sign-out-alt la-6x" {...props} />
)
export const PlusIcon = (props: Props) => (
    <Icon className="las la-plus la-6x" {...props} />
)
export const ExchangeIcon = (props: Props) => (
    <Icon className="las la-exchange-alt la-6x" {...props} />
)
export const ExternalLinkIcon = (props: Props) => (
    <Icon className="las la-external-link-square-alt la-6x" {...props} />
)
export const ToolsIcon = (props: Props) => (
    <Icon className="las la-tools la-6x" {...props} />
)
export const CloudIcon = (props: Props) => (
    <Icon className="las la-cloud la-6x" {...props} />
)
export const ThIcon = (props: Props) => (
    <Icon className="las la-th la-6x" {...props} />
)
export const PaperPlaneIcon = (props: Props) => (
    <Icon className="las la-paper-plane la-6x" {...props} />
)
export const PenIcon = (props: Props) => (
    <Icon className="las la-pen la-6x" {...props} />
)
export const SearchIcon = (props: Props) => (
    <Icon className="las la-search la-6x" {...props} />
)
export const BedIcon = (props: Props) => (
    <Icon className="las la-bed la-6x" {...props} />
)
export const PlayCircleIcon = (props: Props) => (
    <Icon className="las la-play-circle la-6x" {...props} />
)
export const CheckIcon = (props: Props) => (
    <Icon className="las la-check la-6x" {...props} />
)
export const DatabaseIcon = (props: Props) => (
    <Icon className="las la-database la-6x" {...props} />
)
export const CircleIcon = (props: Props) => (
    <Icon className="las la-circle la-6x" {...props} />
)
export const QuestionIcon = (props: Props) => (
    <Icon className="las la-question-circle la-6x" {...props} />
)
