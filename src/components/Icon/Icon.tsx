import React from 'react'
import 'line-awesome/dist/line-awesome/css/line-awesome.min.css'

export interface Props {
    className: string
    size?: string
    color?: string
}

const IconWrapper: React.FC<Props> = ({
    className,
    size = '13px',
    color,
    ...rest
}) => {
    return (
        <i
            className={className}
            style={{ color: color, fontSize: size }}
            {...rest}
        />
    )
}

export { IconWrapper as Icon }
