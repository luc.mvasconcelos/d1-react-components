import React from 'react'

import { Container, Wraper } from './styles'

export interface Props {
    content?: string
    color?: string
    backgroundColor?: string
    animation?: boolean
}

const TooltipWrapper: React.FC<Props> = ({
    color = '#FFFFFF',
    animation,
    backgroundColor = '#3E4157',
    content,
    children,
    ...rest
}) => {
    return (
        <Wraper animation={animation}>
            {children}
            <Container
                backgroundColor={backgroundColor}
                color={color}
                animation={animation}
                {...rest}
            >
                {content}
            </Container>
        </Wraper>
    )
}

export { TooltipWrapper as Tooltip }
