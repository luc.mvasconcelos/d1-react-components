import styled, { css } from 'styled-components'

import { Props } from './index'

export const Wraper = styled.div<Props>`
    display: inline-block;
    position: relative;
    & > div:last-child {
        transition-timing-function: ease-in;
        transition: 0.25s;
        opacity: 0;
        visibility:hidden;
    }
    &:hover > div:last-child {
        visibility: visible;
        opacity: 1;
    }
    ${(props) =>
        props.animation &&
        css`
            & > div:last-child {
                transition-timing-function: ease-in;
                transition: 0.25s;
                opacity: 0;
                transform: translateX(50%) translateY(-50%);
                visibility:hidden;
            }
            &:hover > div:last-child {
                visibility: visible;
                opacity: 1;
                transform: translateX(0) translateY(-50%);
            }
        `}
`

export const Container = styled.div<Props>`





position: absolute;
border-radius: 6px;
left: 50%;
transform: translateX(-50%);
padding: 12px;
color: ${(props) => props.color};
background: ${(props) => props.backgroundColor};
line-height: 1;
z-index: 100;
white-space: nowrap;

&::before{
  content: " ";
  left: 50%;
  border: solid transparent;
  height: 0;
  width: 0;
  position: absolute;
  pointer-events: none;
  border-width: 6px
  margin-left: -6px;

}

left: calc(100% + 6px);
top: 50%;
transform: translateX(0) translateY(-50%);

&::before{
  left: -6px;
  top: 50%;
  transform: translateX(0) translateY(-50%);
  border-right-color: ${(props) => props.backgroundColor};
}




`
