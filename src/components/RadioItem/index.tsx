import React, { Ref } from 'react'

import { Item, RadioInput } from './styles'

export interface Props {
    color?: string
    value?: string | number
    selectedColor?: string
    onChange?: (e: any) => void
    children?: React.ReactNode
    fontSize?: string
    checked?: boolean
}

const ItemWrapper: React.FC<Props> = React.forwardRef(
    (
        {
            children,
            value,
            color = '#3E4157',
            selectedColor = '#117EFF',
            fontSize = '13px',
            checked = false,
            onChange,
            ...rest
        },
        ref: Ref<HTMLInputElement>
    ) => {
        return (
            <Item
                selectedColor={selectedColor}
                color={color}
                fontSize={fontSize}
                checked={checked}
                {...rest}
            >
                <RadioInput
                    type="radio"
                    checked={checked}
                    value={value}
                    onChange={onChange}
                    ref={ref}
                />
                {children}
            </Item>
        )
    }
)

export { ItemWrapper as RadioItem }
