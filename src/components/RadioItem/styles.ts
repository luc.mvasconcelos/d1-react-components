import styled, { css } from 'styled-components'
import { Container as AvatarContainer } from '../Avatar/styles'

import { Props } from './index'

export const Item = styled.div<Props>`
    display: flex;
    align-items: center;
    height: 32px;
    position: relative;
    cursor: pointer;
    *:last-child{
        margin-left: auto;
        order: 3;
    }
    *:nth-child(3){
        margin-left: 10px;
        margin-right: 13px;
    }
    &:hover,
    &:focus {
        color: ${(props) => (props.checked ? '#117EFF' : '#E4E6F1')};
        ${AvatarContainer} {
            background-color: #ebeef6;
            ${(props) =>
                props.checked &&
                css`
                    background-color: #117eff;
                `}
        }
    }

    ${(props) =>
        props.checked &&
        css`
            color: #117eff;
        `}
    ${AvatarContainer} {
        ${(props) =>
            props.checked &&
            css`
                color: #ffffff !important;
                background-color: #117eff;
            `}
    }
`

export const RadioInput = styled.input`
    opacity: 0;
    cursor: pointer;
    z-index: 2;
    width: 100%;
    height: 25px;
    //margin-left: 10px;
    position: absolute;
    top: 0;
    left: 0;
    ${(props) =>
        props.checked &&
        css`
            &:checked ~ * {
                color: #117eff;
            }
        `}
`
