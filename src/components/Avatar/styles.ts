import styled, { css } from 'styled-components'
import { lighten, darken, readableColor, setLightness } from 'polished'

import { Props } from './index'

export const Container = styled.div<Props>`
  width: ${({ size }) => size};
  height: ${({ size }) => size};
  font-weight: bold;
  display: flex;
  overflow: hidden;
  position: relative;
  align-items: center;
  background-color: ${({ backgroundColor }) => backgroundColor};
  color: ${({ color }) => color};
  flex-shrink: 0;
  line-height: 1;
  user-select: none;
  border-radius: 50%;
  justify-content: center;
  &:hover {
    box-shadow: 0 0 11px rgba(33, 33, 33, 0.2);
}
  }
`
