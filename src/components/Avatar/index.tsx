import React from 'react'

import { Container } from './styles'

export interface Props {
    backgroundColor?: string
    color?: string
    size?: string
}

const AvatarWrapper: React.FC<Props> = ({
    children,
    backgroundColor = '#117EFF',
    color = '#fff',
    size = '24px',
    ...rest
}) => {
    return (
        <Container
            backgroundColor={backgroundColor}
            color={color}
            size={size}
            {...rest}
        >
            {children}
        </Container>
    )
}

export { AvatarWrapper as Avatar }
